# Paasword Angular Client Librery

[Paasword](https://www.paas-word.com) is an online authentication and user management service.
This Angular client librery for the [Paasword](https://www.paas-word.com) enables angular developers to manage their authenticated users locally. 

## Usage

1. Create a free account at [Paasword](https://www.paas-word.com) website.
2. Recieve a login, sign-up, account and forgot-password pages for your website based on the user attributes you set up.
3. Set the callback pages on your website where users will be redirected after they sign-up and log in. 
4. Once a user is redirected to your angular website you could use this library to guard routes against unauthenticated users, get the user's information, update it and log him out. 

## Installation

`npm install paasword --save`

## Add to providers
in app.module.ts:

```javascript
import { PaaswordService } from 'paasword';

@NgModule({
    ...
    providers: [
        PaaswordService,
        ...
    ]
})
```

## Gaurd routes against unauthenticated users
You should place these gaurds on the routes of the callback URLs you set up on the website. 

```javascript
import { PaaswordService } from 'paasword';
const routes: Routes = [
        ....
    	{ path: 'private-page', component: PrivatePageComponent, canActivate: [PaaswordService] },
    	{ path: 'dashboard', component: DashboardComponent, canActivate: [PaaswordService] },
    	{ path: 'public-page', component: PublicPageComponent },
        ....`
```

## Get user information and update him

```javascript
import { PaaswordService } from 'paasword';
export class MyService {
	
    constructor(private paaswordService: PaaswordService) { }
    
    myFunction() {
        //get user information
        var user = this.paaswordService.getCurrentUser();
        
        //update user
        user.favorite_color = 'red';
        this.paaswordService.updateCurrentUser(user);
        
        //if anyone is listening for changes
        this.paaswordService.userChanged.emit(user);
    }
}
```

## Listen to user changes

```javascript
import { PaaswordService } from 'paasword';

export class MyComponent implements OnInit {
	user = {};
	constructor(private paaswordService: PaaswordService) { 
		paaswordService.userChanged.subscribe(user => {
			this.user = user;
		});
	}
}
```

## Check if user is authenticated and logout
```javascript
import { PaaswordService } from 'paasword';

export class MyComponent implements OnInit {

	constructor(private paaswordService: PaaswordService) { }

	logout() {
        if (this.paaswordService.isAuthenticated()) {
            this.paaswordService.logout()
        }
    }
}
```
