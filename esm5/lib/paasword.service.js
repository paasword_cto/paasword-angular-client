/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, EventEmitter, Output } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import * as jwt_decode_ from "jwt-decode";
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
/** @type {?} */
var jwt_decode = jwt_decode_;
var PaaswordService = /** @class */ (function () {
    function PaaswordService(router, route) {
        this.router = router;
        this.route = route;
        this.userChanged = new EventEmitter();
    }
    /**
     * @return {?}
     */
    PaaswordService.prototype.canActivate = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var url = new URL(window.location.href);
        /** @type {?} */
        var logout = url.searchParams.get("paasword-logout");
        if (logout) {
            this.logout();
        }
        /** @type {?} */
        var token = url.searchParams.get("paasword-token");
        if (token) {
            this.login(token);
            this.router.navigate([url.pathname]);
        }
        /** @type {?} */
        var user = this.getCurrentUser();
        if (user && user.AutoLogout.IsEnabled) {
            /** @type {?} */
            var hoursAfter = user.AutoLogout.LogoutEveryXHours;
            /** @type {?} */
            var now = Math.floor((new Date).getTime() / 1000);
            if (user.iat + hoursAfter * 3600 > now) {
                this.logout();
            }
        }
        return this.isAuthenticated();
    };
    /**
     * @return {?}
     */
    PaaswordService.prototype.isAuthenticated = /**
     * @return {?}
     */
    function () {
        return localStorage.getItem('currentUser') != null;
    };
    /**
     * @return {?}
     */
    PaaswordService.prototype.logout = /**
     * @return {?}
     */
    function () {
        localStorage.removeItem('currentUser');
        this.userChanged.emit(null);
        this.router.navigate(['/']);
    };
    /**
     * @param {?} token
     * @return {?}
     */
    PaaswordService.prototype.login = /**
     * @param {?} token
     * @return {?}
     */
    function (token) {
        /** @type {?} */
        var decoded = jwt_decode(token);
        decoded.token = token;
        localStorage.setItem('currentUser', JSON.stringify(decoded));
        this.userChanged.emit(decoded);
    };
    /**
     * @return {?}
     */
    PaaswordService.prototype.getCurrentUser = /**
     * @return {?}
     */
    function () {
        return JSON.parse(localStorage.getItem('currentUser'));
    };
    /**
     * @param {?} user
     * @return {?}
     */
    PaaswordService.prototype.updateCurrentUser = /**
     * @param {?} user
     * @return {?}
     */
    function (user) {
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.userChanged.emit(user);
    };
    /**
     * @return {?}
     */
    PaaswordService.prototype.getHttpHeader = /**
     * @return {?}
     */
    function () {
        // create authorization header with jwt token
        /** @type {?} */
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser && currentUser.token) {
            /** @type {?} */
            var headers = new Headers({ 'x-auth-token': currentUser.token });
            return new RequestOptions({ headers: headers });
        }
    };
    PaaswordService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    PaaswordService.ctorParameters = function () { return [
        { type: Router },
        { type: ActivatedRoute }
    ]; };
    PaaswordService.propDecorators = {
        userChanged: [{ type: Output }]
    };
    /** @nocollapse */ PaaswordService.ngInjectableDef = i0.defineInjectable({ factory: function PaaswordService_Factory() { return new PaaswordService(i0.inject(i1.Router), i0.inject(i1.ActivatedRoute)); }, token: PaaswordService, providedIn: "root" });
    return PaaswordService;
}());
export { PaaswordService };
if (false) {
    /** @type {?} */
    PaaswordService.prototype.userChanged;
    /** @type {?} */
    PaaswordService.prototype.router;
    /**
     * @type {?}
     * @private
     */
    PaaswordService.prototype.route;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFhc3dvcmQuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3BhYXN3b3JkLyIsInNvdXJjZXMiOlsibGliL3BhYXN3b3JkLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNqRSxPQUFPLEVBQUUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN4RCxPQUFPLEVBQUUsTUFBTSxFQUFlLGNBQWMsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQ3RFLE9BQU8sS0FBSyxXQUFXLE1BQU0sWUFBWSxDQUFDOzs7O0lBQ3BDLFVBQVUsR0FBRyxXQUFXO0FBRTlCO0lBT0MseUJBQW1CLE1BQWMsRUFBVSxLQUFxQjtRQUE3QyxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQVUsVUFBSyxHQUFMLEtBQUssQ0FBZ0I7UUFGdEQsZ0JBQVcsR0FBc0IsSUFBSSxZQUFZLEVBQUUsQ0FBQztJQUVLLENBQUM7Ozs7SUFFcEUscUNBQVc7OztJQUFYOztZQUVLLEdBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQzs7WUFDbkMsTUFBTSxHQUFHLEdBQUcsQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFDO1FBRXBELElBQUksTUFBTSxFQUFFO1lBQ1gsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO1NBQ2Q7O1lBRUcsS0FBSyxHQUFHLEdBQUcsQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDO1FBQ2xELElBQUksS0FBSyxFQUFFO1lBQ1YsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNsQixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1NBQ3JDOztZQUVHLElBQUksR0FBRyxJQUFJLENBQUMsY0FBYyxFQUFFO1FBQ2hDLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxFQUFFOztnQkFDbEMsVUFBVSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCOztnQkFDOUMsR0FBRyxHQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxDQUFDLE9BQU8sRUFBRSxHQUFDLElBQUksQ0FBQztZQUNoRCxJQUFJLElBQUksQ0FBQyxHQUFHLEdBQUcsVUFBVSxHQUFHLElBQUksR0FBRyxHQUFHLEVBQUU7Z0JBQ3ZDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQzthQUNkO1NBQ0Q7UUFFRCxPQUFPLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztJQUMvQixDQUFDOzs7O0lBRUQseUNBQWU7OztJQUFmO1FBQ0MsT0FBTyxZQUFZLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxJQUFJLElBQUksQ0FBQztJQUNwRCxDQUFDOzs7O0lBRUQsZ0NBQU07OztJQUFOO1FBQ0MsWUFBWSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUN2QyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1QixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDN0IsQ0FBQzs7Ozs7SUFFRCwrQkFBSzs7OztJQUFMLFVBQU0sS0FBSzs7WUFDTixPQUFPLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQztRQUMvQixPQUFPLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUN0QixZQUFZLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7UUFDN0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDaEMsQ0FBQzs7OztJQUVELHdDQUFjOzs7SUFBZDtRQUNDLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7SUFDeEQsQ0FBQzs7Ozs7SUFFRCwyQ0FBaUI7Ozs7SUFBakIsVUFBa0IsSUFBSTtRQUNyQixZQUFZLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDMUQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDN0IsQ0FBQzs7OztJQUVELHVDQUFhOzs7SUFBYjs7O1lBRUssV0FBVyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNqRSxJQUFJLFdBQVcsSUFBSSxXQUFXLENBQUMsS0FBSyxFQUFFOztnQkFDakMsT0FBTyxHQUFHLElBQUksT0FBTyxDQUFDLEVBQUUsY0FBYyxFQUFFLFdBQVcsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUNoRSxPQUFPLElBQUksY0FBYyxDQUFDLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUM7U0FDaEQ7SUFDRixDQUFDOztnQkFyRUQsVUFBVSxTQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO2lCQUNuQjs7OztnQkFOUSxNQUFNO2dCQUFlLGNBQWM7Ozs4QkFTMUMsTUFBTTs7OzBCQVhSO0NBNEVDLEFBdEVELElBc0VDO1NBbkVZLGVBQWU7OztJQUUzQixzQ0FBOEQ7O0lBRWxELGlDQUFxQjs7Ozs7SUFBRSxnQ0FBNkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBFdmVudEVtaXR0ZXIsIE91dHB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSGVhZGVycywgUmVxdWVzdE9wdGlvbnMgfSBmcm9tICdAYW5ndWxhci9odHRwJztcbmltcG9ydCB7IFJvdXRlciwgQ2FuQWN0aXZhdGUsIEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCAqIGFzIGp3dF9kZWNvZGVfIGZyb20gXCJqd3QtZGVjb2RlXCI7XG5jb25zdCBqd3RfZGVjb2RlID0gand0X2RlY29kZV87XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIFBhYXN3b3JkU2VydmljZSBpbXBsZW1lbnRzIENhbkFjdGl2YXRlIHtcblxuXHRAT3V0cHV0KCkgdXNlckNoYW5nZWQ6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG5cdGNvbnN0cnVjdG9yKHB1YmxpYyByb3V0ZXI6IFJvdXRlciwgcHJpdmF0ZSByb3V0ZTogQWN0aXZhdGVkUm91dGUpIHt9XG5cblx0Y2FuQWN0aXZhdGUoKTogYm9vbGVhbiB7XG5cblx0XHR2YXIgdXJsID0gbmV3IFVSTCh3aW5kb3cubG9jYXRpb24uaHJlZik7XG5cdFx0dmFyIGxvZ291dCA9IHVybC5zZWFyY2hQYXJhbXMuZ2V0KFwicGFhc3dvcmQtbG9nb3V0XCIpO1xuXG5cdFx0aWYgKGxvZ291dCkge1xuXHRcdFx0dGhpcy5sb2dvdXQoKTtcblx0XHR9XG5cblx0XHR2YXIgdG9rZW4gPSB1cmwuc2VhcmNoUGFyYW1zLmdldChcInBhYXN3b3JkLXRva2VuXCIpO1xuXHRcdGlmICh0b2tlbikge1xuXHRcdFx0dGhpcy5sb2dpbih0b2tlbik7XG5cdFx0XHR0aGlzLnJvdXRlci5uYXZpZ2F0ZShbdXJsLnBhdGhuYW1lXSk7XG5cdFx0fVxuXG5cdFx0dmFyIHVzZXIgPSB0aGlzLmdldEN1cnJlbnRVc2VyKCk7XG5cdFx0aWYgKHVzZXIgJiYgdXNlci5BdXRvTG9nb3V0LklzRW5hYmxlZCkge1xuXHRcdFx0dmFyIGhvdXJzQWZ0ZXIgPSB1c2VyLkF1dG9Mb2dvdXQuTG9nb3V0RXZlcnlYSG91cnM7XG5cdFx0XHR2YXIgbm93ID0gIE1hdGguZmxvb3IoKG5ldyBEYXRlKS5nZXRUaW1lKCkvMTAwMCk7XG5cdFx0XHRpZiAodXNlci5pYXQgKyBob3Vyc0FmdGVyICogMzYwMCA+IG5vdykge1xuXHRcdFx0XHR0aGlzLmxvZ291dCgpO1xuXHRcdFx0fVxuXHRcdH1cblxuXHRcdHJldHVybiB0aGlzLmlzQXV0aGVudGljYXRlZCgpO1xuXHR9XG5cblx0aXNBdXRoZW50aWNhdGVkKCk6IGJvb2xlYW4ge1xuXHRcdHJldHVybiBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnY3VycmVudFVzZXInKSAhPSBudWxsO1xuXHR9XG5cblx0bG9nb3V0KCkge1xuXHRcdGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKCdjdXJyZW50VXNlcicpO1xuXHRcdHRoaXMudXNlckNoYW5nZWQuZW1pdChudWxsKTtcblx0XHR0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJy8nXSk7XG5cdH1cblxuXHRsb2dpbih0b2tlbikge1xuXHRcdHZhciBkZWNvZGVkID0gand0X2RlY29kZSh0b2tlbik7XG5cdFx0ZGVjb2RlZC50b2tlbiA9IHRva2VuO1xuXHRcdGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdjdXJyZW50VXNlcicsIEpTT04uc3RyaW5naWZ5KGRlY29kZWQpKTtcblx0XHR0aGlzLnVzZXJDaGFuZ2VkLmVtaXQoZGVjb2RlZCk7XG5cdH1cblxuXHRnZXRDdXJyZW50VXNlcigpIHtcblx0XHRyZXR1cm4gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnY3VycmVudFVzZXInKSk7XG5cdH1cblxuXHR1cGRhdGVDdXJyZW50VXNlcih1c2VyKSB7XG5cdFx0bG9jYWxTdG9yYWdlLnNldEl0ZW0oJ2N1cnJlbnRVc2VyJywgSlNPTi5zdHJpbmdpZnkodXNlcikpO1xuXHRcdHRoaXMudXNlckNoYW5nZWQuZW1pdCh1c2VyKTtcblx0fVxuXG5cdGdldEh0dHBIZWFkZXIoKSB7XG5cdFx0Ly8gY3JlYXRlIGF1dGhvcml6YXRpb24gaGVhZGVyIHdpdGggand0IHRva2VuXG5cdFx0bGV0IGN1cnJlbnRVc2VyID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnY3VycmVudFVzZXInKSk7XG5cdFx0aWYgKGN1cnJlbnRVc2VyICYmIGN1cnJlbnRVc2VyLnRva2VuKSB7XG5cdFx0XHRsZXQgaGVhZGVycyA9IG5ldyBIZWFkZXJzKHsgJ3gtYXV0aC10b2tlbic6IGN1cnJlbnRVc2VyLnRva2VuIH0pO1xuXHRcdFx0cmV0dXJuIG5ldyBSZXF1ZXN0T3B0aW9ucyh7IGhlYWRlcnM6IGhlYWRlcnMgfSk7XG5cdFx0fVxuXHR9XG59XG4iXX0=