import { Injectable, EventEmitter, Output, defineInjectable, inject } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import * as jwt_decode_ from 'jwt-decode';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const jwt_decode = jwt_decode_;
class PaaswordService {
    /**
     * @param {?} router
     * @param {?} route
     */
    constructor(router, route) {
        this.router = router;
        this.route = route;
        this.userChanged = new EventEmitter();
    }
    /**
     * @return {?}
     */
    canActivate() {
        /** @type {?} */
        var url = new URL(window.location.href);
        /** @type {?} */
        var logout = url.searchParams.get("paasword-logout");
        if (logout) {
            this.logout();
        }
        /** @type {?} */
        var token = url.searchParams.get("paasword-token");
        if (token) {
            this.login(token);
            this.router.navigate([url.pathname]);
        }
        /** @type {?} */
        var user = this.getCurrentUser();
        if (user && user.AutoLogout.IsEnabled) {
            /** @type {?} */
            var hoursAfter = user.AutoLogout.LogoutEveryXHours;
            /** @type {?} */
            var now = Math.floor((new Date).getTime() / 1000);
            if (user.iat + hoursAfter * 3600 > now) {
                this.logout();
            }
        }
        return this.isAuthenticated();
    }
    /**
     * @return {?}
     */
    isAuthenticated() {
        return localStorage.getItem('currentUser') != null;
    }
    /**
     * @return {?}
     */
    logout() {
        localStorage.removeItem('currentUser');
        this.userChanged.emit(null);
        this.router.navigate(['/']);
    }
    /**
     * @param {?} token
     * @return {?}
     */
    login(token) {
        /** @type {?} */
        var decoded = jwt_decode(token);
        decoded.token = token;
        localStorage.setItem('currentUser', JSON.stringify(decoded));
        this.userChanged.emit(decoded);
    }
    /**
     * @return {?}
     */
    getCurrentUser() {
        return JSON.parse(localStorage.getItem('currentUser'));
    }
    /**
     * @param {?} user
     * @return {?}
     */
    updateCurrentUser(user) {
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.userChanged.emit(user);
    }
    /**
     * @return {?}
     */
    getHttpHeader() {
        // create authorization header with jwt token
        /** @type {?} */
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser && currentUser.token) {
            /** @type {?} */
            let headers = new Headers({ 'x-auth-token': currentUser.token });
            return new RequestOptions({ headers: headers });
        }
    }
}
PaaswordService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
PaaswordService.ctorParameters = () => [
    { type: Router },
    { type: ActivatedRoute }
];
PaaswordService.propDecorators = {
    userChanged: [{ type: Output }]
};
/** @nocollapse */ PaaswordService.ngInjectableDef = defineInjectable({ factory: function PaaswordService_Factory() { return new PaaswordService(inject(Router), inject(ActivatedRoute)); }, token: PaaswordService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { PaaswordService };

//# sourceMappingURL=paasword.js.map