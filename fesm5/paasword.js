import { Injectable, EventEmitter, Output, defineInjectable, inject } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import * as jwt_decode_ from 'jwt-decode';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
var jwt_decode = jwt_decode_;
var PaaswordService = /** @class */ (function () {
    function PaaswordService(router, route) {
        this.router = router;
        this.route = route;
        this.userChanged = new EventEmitter();
    }
    /**
     * @return {?}
     */
    PaaswordService.prototype.canActivate = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var url = new URL(window.location.href);
        /** @type {?} */
        var logout = url.searchParams.get("paasword-logout");
        if (logout) {
            this.logout();
        }
        /** @type {?} */
        var token = url.searchParams.get("paasword-token");
        if (token) {
            this.login(token);
            this.router.navigate([url.pathname]);
        }
        /** @type {?} */
        var user = this.getCurrentUser();
        if (user && user.AutoLogout.IsEnabled) {
            /** @type {?} */
            var hoursAfter = user.AutoLogout.LogoutEveryXHours;
            /** @type {?} */
            var now = Math.floor((new Date).getTime() / 1000);
            if (user.iat + hoursAfter * 3600 > now) {
                this.logout();
            }
        }
        return this.isAuthenticated();
    };
    /**
     * @return {?}
     */
    PaaswordService.prototype.isAuthenticated = /**
     * @return {?}
     */
    function () {
        return localStorage.getItem('currentUser') != null;
    };
    /**
     * @return {?}
     */
    PaaswordService.prototype.logout = /**
     * @return {?}
     */
    function () {
        localStorage.removeItem('currentUser');
        this.userChanged.emit(null);
        this.router.navigate(['/']);
    };
    /**
     * @param {?} token
     * @return {?}
     */
    PaaswordService.prototype.login = /**
     * @param {?} token
     * @return {?}
     */
    function (token) {
        /** @type {?} */
        var decoded = jwt_decode(token);
        decoded.token = token;
        localStorage.setItem('currentUser', JSON.stringify(decoded));
        this.userChanged.emit(decoded);
    };
    /**
     * @return {?}
     */
    PaaswordService.prototype.getCurrentUser = /**
     * @return {?}
     */
    function () {
        return JSON.parse(localStorage.getItem('currentUser'));
    };
    /**
     * @param {?} user
     * @return {?}
     */
    PaaswordService.prototype.updateCurrentUser = /**
     * @param {?} user
     * @return {?}
     */
    function (user) {
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.userChanged.emit(user);
    };
    /**
     * @return {?}
     */
    PaaswordService.prototype.getHttpHeader = /**
     * @return {?}
     */
    function () {
        // create authorization header with jwt token
        /** @type {?} */
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser && currentUser.token) {
            /** @type {?} */
            var headers = new Headers({ 'x-auth-token': currentUser.token });
            return new RequestOptions({ headers: headers });
        }
    };
    PaaswordService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    PaaswordService.ctorParameters = function () { return [
        { type: Router },
        { type: ActivatedRoute }
    ]; };
    PaaswordService.propDecorators = {
        userChanged: [{ type: Output }]
    };
    /** @nocollapse */ PaaswordService.ngInjectableDef = defineInjectable({ factory: function PaaswordService_Factory() { return new PaaswordService(inject(Router), inject(ActivatedRoute)); }, token: PaaswordService, providedIn: "root" });
    return PaaswordService;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { PaaswordService };

//# sourceMappingURL=paasword.js.map