(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/http'), require('@angular/router'), require('jwt-decode')) :
    typeof define === 'function' && define.amd ? define('paasword', ['exports', '@angular/core', '@angular/http', '@angular/router', 'jwt-decode'], factory) :
    (factory((global.paasword = {}),global.ng.core,global.ng.http,global.ng.router,global.jwt_decode_));
}(this, (function (exports,i0,http,i1,jwt_decode_) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var jwt_decode = jwt_decode_;
    var PaaswordService = /** @class */ (function () {
        function PaaswordService(router, route) {
            this.router = router;
            this.route = route;
            this.userChanged = new i0.EventEmitter();
        }
        /**
         * @return {?}
         */
        PaaswordService.prototype.canActivate = /**
         * @return {?}
         */
            function () {
                /** @type {?} */
                var url = new URL(window.location.href);
                /** @type {?} */
                var logout = url.searchParams.get("paasword-logout");
                if (logout) {
                    this.logout();
                }
                /** @type {?} */
                var token = url.searchParams.get("paasword-token");
                if (token) {
                    this.login(token);
                    this.router.navigate([url.pathname]);
                }
                /** @type {?} */
                var user = this.getCurrentUser();
                if (user && user.AutoLogout.IsEnabled) {
                    /** @type {?} */
                    var hoursAfter = user.AutoLogout.LogoutEveryXHours;
                    /** @type {?} */
                    var now = Math.floor((new Date).getTime() / 1000);
                    if (user.iat + hoursAfter * 3600 > now) {
                        this.logout();
                    }
                }
                return this.isAuthenticated();
            };
        /**
         * @return {?}
         */
        PaaswordService.prototype.isAuthenticated = /**
         * @return {?}
         */
            function () {
                return localStorage.getItem('currentUser') != null;
            };
        /**
         * @return {?}
         */
        PaaswordService.prototype.logout = /**
         * @return {?}
         */
            function () {
                localStorage.removeItem('currentUser');
                this.userChanged.emit(null);
                this.router.navigate(['/']);
            };
        /**
         * @param {?} token
         * @return {?}
         */
        PaaswordService.prototype.login = /**
         * @param {?} token
         * @return {?}
         */
            function (token) {
                /** @type {?} */
                var decoded = jwt_decode(token);
                decoded.token = token;
                localStorage.setItem('currentUser', JSON.stringify(decoded));
                this.userChanged.emit(decoded);
            };
        /**
         * @return {?}
         */
        PaaswordService.prototype.getCurrentUser = /**
         * @return {?}
         */
            function () {
                return JSON.parse(localStorage.getItem('currentUser'));
            };
        /**
         * @param {?} user
         * @return {?}
         */
        PaaswordService.prototype.updateCurrentUser = /**
         * @param {?} user
         * @return {?}
         */
            function (user) {
                localStorage.setItem('currentUser', JSON.stringify(user));
                this.userChanged.emit(user);
            };
        /**
         * @return {?}
         */
        PaaswordService.prototype.getHttpHeader = /**
         * @return {?}
         */
            function () {
                // create authorization header with jwt token
                /** @type {?} */
                var currentUser = JSON.parse(localStorage.getItem('currentUser'));
                if (currentUser && currentUser.token) {
                    /** @type {?} */
                    var headers = new http.Headers({ 'x-auth-token': currentUser.token });
                    return new http.RequestOptions({ headers: headers });
                }
            };
        PaaswordService.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        PaaswordService.ctorParameters = function () {
            return [
                { type: i1.Router },
                { type: i1.ActivatedRoute }
            ];
        };
        PaaswordService.propDecorators = {
            userChanged: [{ type: i0.Output }]
        };
        /** @nocollapse */ PaaswordService.ngInjectableDef = i0.defineInjectable({ factory: function PaaswordService_Factory() { return new PaaswordService(i0.inject(i1.Router), i0.inject(i1.ActivatedRoute)); }, token: PaaswordService, providedIn: "root" });
        return PaaswordService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    exports.PaaswordService = PaaswordService;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=paasword.umd.js.map