import { EventEmitter } from '@angular/core';
import { RequestOptions } from '@angular/http';
import { Router, CanActivate, ActivatedRoute } from '@angular/router';
export declare class PaaswordService implements CanActivate {
    router: Router;
    private route;
    userChanged: EventEmitter<any>;
    constructor(router: Router, route: ActivatedRoute);
    canActivate(): boolean;
    isAuthenticated(): boolean;
    logout(): void;
    login(token: any): void;
    getCurrentUser(): any;
    updateCurrentUser(user: any): void;
    getHttpHeader(): RequestOptions;
}
