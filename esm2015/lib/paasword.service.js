/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, EventEmitter, Output } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import * as jwt_decode_ from "jwt-decode";
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
/** @type {?} */
const jwt_decode = jwt_decode_;
export class PaaswordService {
    /**
     * @param {?} router
     * @param {?} route
     */
    constructor(router, route) {
        this.router = router;
        this.route = route;
        this.userChanged = new EventEmitter();
    }
    /**
     * @return {?}
     */
    canActivate() {
        /** @type {?} */
        var url = new URL(window.location.href);
        /** @type {?} */
        var logout = url.searchParams.get("paasword-logout");
        if (logout) {
            this.logout();
        }
        /** @type {?} */
        var token = url.searchParams.get("paasword-token");
        if (token) {
            this.login(token);
            this.router.navigate([url.pathname]);
        }
        /** @type {?} */
        var user = this.getCurrentUser();
        if (user && user.AutoLogout.IsEnabled) {
            /** @type {?} */
            var hoursAfter = user.AutoLogout.LogoutEveryXHours;
            /** @type {?} */
            var now = Math.floor((new Date).getTime() / 1000);
            if (user.iat + hoursAfter * 3600 > now) {
                this.logout();
            }
        }
        return this.isAuthenticated();
    }
    /**
     * @return {?}
     */
    isAuthenticated() {
        return localStorage.getItem('currentUser') != null;
    }
    /**
     * @return {?}
     */
    logout() {
        localStorage.removeItem('currentUser');
        this.userChanged.emit(null);
        this.router.navigate(['/']);
    }
    /**
     * @param {?} token
     * @return {?}
     */
    login(token) {
        /** @type {?} */
        var decoded = jwt_decode(token);
        decoded.token = token;
        localStorage.setItem('currentUser', JSON.stringify(decoded));
        this.userChanged.emit(decoded);
    }
    /**
     * @return {?}
     */
    getCurrentUser() {
        return JSON.parse(localStorage.getItem('currentUser'));
    }
    /**
     * @param {?} user
     * @return {?}
     */
    updateCurrentUser(user) {
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.userChanged.emit(user);
    }
    /**
     * @return {?}
     */
    getHttpHeader() {
        // create authorization header with jwt token
        /** @type {?} */
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser && currentUser.token) {
            /** @type {?} */
            let headers = new Headers({ 'x-auth-token': currentUser.token });
            return new RequestOptions({ headers: headers });
        }
    }
}
PaaswordService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
PaaswordService.ctorParameters = () => [
    { type: Router },
    { type: ActivatedRoute }
];
PaaswordService.propDecorators = {
    userChanged: [{ type: Output }]
};
/** @nocollapse */ PaaswordService.ngInjectableDef = i0.defineInjectable({ factory: function PaaswordService_Factory() { return new PaaswordService(i0.inject(i1.Router), i0.inject(i1.ActivatedRoute)); }, token: PaaswordService, providedIn: "root" });
if (false) {
    /** @type {?} */
    PaaswordService.prototype.userChanged;
    /** @type {?} */
    PaaswordService.prototype.router;
    /**
     * @type {?}
     * @private
     */
    PaaswordService.prototype.route;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFhc3dvcmQuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3BhYXN3b3JkLyIsInNvdXJjZXMiOlsibGliL3BhYXN3b3JkLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNqRSxPQUFPLEVBQUUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN4RCxPQUFPLEVBQUUsTUFBTSxFQUFlLGNBQWMsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQ3RFLE9BQU8sS0FBSyxXQUFXLE1BQU0sWUFBWSxDQUFDOzs7O01BQ3BDLFVBQVUsR0FBRyxXQUFXO0FBSzlCLE1BQU0sT0FBTyxlQUFlOzs7OztJQUkzQixZQUFtQixNQUFjLEVBQVUsS0FBcUI7UUFBN0MsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUFVLFVBQUssR0FBTCxLQUFLLENBQWdCO1FBRnRELGdCQUFXLEdBQXNCLElBQUksWUFBWSxFQUFFLENBQUM7SUFFSyxDQUFDOzs7O0lBRXBFLFdBQVc7O1lBRU4sR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDOztZQUNuQyxNQUFNLEdBQUcsR0FBRyxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsaUJBQWlCLENBQUM7UUFFcEQsSUFBSSxNQUFNLEVBQUU7WUFDWCxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7U0FDZDs7WUFFRyxLQUFLLEdBQUcsR0FBRyxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUM7UUFDbEQsSUFBSSxLQUFLLEVBQUU7WUFDVixJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2xCLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7U0FDckM7O1lBRUcsSUFBSSxHQUFHLElBQUksQ0FBQyxjQUFjLEVBQUU7UUFDaEMsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEVBQUU7O2dCQUNsQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUI7O2dCQUM5QyxHQUFHLEdBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLENBQUMsT0FBTyxFQUFFLEdBQUMsSUFBSSxDQUFDO1lBQ2hELElBQUksSUFBSSxDQUFDLEdBQUcsR0FBRyxVQUFVLEdBQUcsSUFBSSxHQUFHLEdBQUcsRUFBRTtnQkFDdkMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO2FBQ2Q7U0FDRDtRQUVELE9BQU8sSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO0lBQy9CLENBQUM7Ozs7SUFFRCxlQUFlO1FBQ2QsT0FBTyxZQUFZLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxJQUFJLElBQUksQ0FBQztJQUNwRCxDQUFDOzs7O0lBRUQsTUFBTTtRQUNMLFlBQVksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDdkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDNUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQzdCLENBQUM7Ozs7O0lBRUQsS0FBSyxDQUFDLEtBQUs7O1lBQ04sT0FBTyxHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUM7UUFDL0IsT0FBTyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDdEIsWUFBWSxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1FBQzdELElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ2hDLENBQUM7Ozs7SUFFRCxjQUFjO1FBQ2IsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQztJQUN4RCxDQUFDOzs7OztJQUVELGlCQUFpQixDQUFDLElBQUk7UUFDckIsWUFBWSxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBQzFELElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzdCLENBQUM7Ozs7SUFFRCxhQUFhOzs7WUFFUixXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ2pFLElBQUksV0FBVyxJQUFJLFdBQVcsQ0FBQyxLQUFLLEVBQUU7O2dCQUNqQyxPQUFPLEdBQUcsSUFBSSxPQUFPLENBQUMsRUFBRSxjQUFjLEVBQUUsV0FBVyxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ2hFLE9BQU8sSUFBSSxjQUFjLENBQUMsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLENBQUMsQ0FBQztTQUNoRDtJQUNGLENBQUM7OztZQXJFRCxVQUFVLFNBQUM7Z0JBQ1YsVUFBVSxFQUFFLE1BQU07YUFDbkI7Ozs7WUFOUSxNQUFNO1lBQWUsY0FBYzs7OzBCQVMxQyxNQUFNOzs7OztJQUFQLHNDQUE4RDs7SUFFbEQsaUNBQXFCOzs7OztJQUFFLGdDQUE2QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIEV2ZW50RW1pdHRlciwgT3V0cHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIZWFkZXJzLCBSZXF1ZXN0T3B0aW9ucyB9IGZyb20gJ0Bhbmd1bGFyL2h0dHAnO1xuaW1wb3J0IHsgUm91dGVyLCBDYW5BY3RpdmF0ZSwgQWN0aXZhdGVkUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0ICogYXMgand0X2RlY29kZV8gZnJvbSBcImp3dC1kZWNvZGVcIjtcbmNvbnN0IGp3dF9kZWNvZGUgPSBqd3RfZGVjb2RlXztcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgUGFhc3dvcmRTZXJ2aWNlIGltcGxlbWVudHMgQ2FuQWN0aXZhdGUge1xuXG5cdEBPdXRwdXQoKSB1c2VyQ2hhbmdlZDogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cblx0Y29uc3RydWN0b3IocHVibGljIHJvdXRlcjogUm91dGVyLCBwcml2YXRlIHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSkge31cblxuXHRjYW5BY3RpdmF0ZSgpOiBib29sZWFuIHtcblxuXHRcdHZhciB1cmwgPSBuZXcgVVJMKHdpbmRvdy5sb2NhdGlvbi5ocmVmKTtcblx0XHR2YXIgbG9nb3V0ID0gdXJsLnNlYXJjaFBhcmFtcy5nZXQoXCJwYWFzd29yZC1sb2dvdXRcIik7XG5cblx0XHRpZiAobG9nb3V0KSB7XG5cdFx0XHR0aGlzLmxvZ291dCgpO1xuXHRcdH1cblxuXHRcdHZhciB0b2tlbiA9IHVybC5zZWFyY2hQYXJhbXMuZ2V0KFwicGFhc3dvcmQtdG9rZW5cIik7XG5cdFx0aWYgKHRva2VuKSB7XG5cdFx0XHR0aGlzLmxvZ2luKHRva2VuKTtcblx0XHRcdHRoaXMucm91dGVyLm5hdmlnYXRlKFt1cmwucGF0aG5hbWVdKTtcblx0XHR9XG5cblx0XHR2YXIgdXNlciA9IHRoaXMuZ2V0Q3VycmVudFVzZXIoKTtcblx0XHRpZiAodXNlciAmJiB1c2VyLkF1dG9Mb2dvdXQuSXNFbmFibGVkKSB7XG5cdFx0XHR2YXIgaG91cnNBZnRlciA9IHVzZXIuQXV0b0xvZ291dC5Mb2dvdXRFdmVyeVhIb3Vycztcblx0XHRcdHZhciBub3cgPSAgTWF0aC5mbG9vcigobmV3IERhdGUpLmdldFRpbWUoKS8xMDAwKTtcblx0XHRcdGlmICh1c2VyLmlhdCArIGhvdXJzQWZ0ZXIgKiAzNjAwID4gbm93KSB7XG5cdFx0XHRcdHRoaXMubG9nb3V0KCk7XG5cdFx0XHR9XG5cdFx0fVxuXG5cdFx0cmV0dXJuIHRoaXMuaXNBdXRoZW50aWNhdGVkKCk7XG5cdH1cblxuXHRpc0F1dGhlbnRpY2F0ZWQoKTogYm9vbGVhbiB7XG5cdFx0cmV0dXJuIGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdjdXJyZW50VXNlcicpICE9IG51bGw7XG5cdH1cblxuXHRsb2dvdXQoKSB7XG5cdFx0bG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oJ2N1cnJlbnRVc2VyJyk7XG5cdFx0dGhpcy51c2VyQ2hhbmdlZC5lbWl0KG51bGwpO1xuXHRcdHRoaXMucm91dGVyLm5hdmlnYXRlKFsnLyddKTtcblx0fVxuXG5cdGxvZ2luKHRva2VuKSB7XG5cdFx0dmFyIGRlY29kZWQgPSBqd3RfZGVjb2RlKHRva2VuKTtcblx0XHRkZWNvZGVkLnRva2VuID0gdG9rZW47XG5cdFx0bG9jYWxTdG9yYWdlLnNldEl0ZW0oJ2N1cnJlbnRVc2VyJywgSlNPTi5zdHJpbmdpZnkoZGVjb2RlZCkpO1xuXHRcdHRoaXMudXNlckNoYW5nZWQuZW1pdChkZWNvZGVkKTtcblx0fVxuXG5cdGdldEN1cnJlbnRVc2VyKCkge1xuXHRcdHJldHVybiBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdjdXJyZW50VXNlcicpKTtcblx0fVxuXG5cdHVwZGF0ZUN1cnJlbnRVc2VyKHVzZXIpIHtcblx0XHRsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnY3VycmVudFVzZXInLCBKU09OLnN0cmluZ2lmeSh1c2VyKSk7XG5cdFx0dGhpcy51c2VyQ2hhbmdlZC5lbWl0KHVzZXIpO1xuXHR9XG5cblx0Z2V0SHR0cEhlYWRlcigpIHtcblx0XHQvLyBjcmVhdGUgYXV0aG9yaXphdGlvbiBoZWFkZXIgd2l0aCBqd3QgdG9rZW5cblx0XHRsZXQgY3VycmVudFVzZXIgPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdjdXJyZW50VXNlcicpKTtcblx0XHRpZiAoY3VycmVudFVzZXIgJiYgY3VycmVudFVzZXIudG9rZW4pIHtcblx0XHRcdGxldCBoZWFkZXJzID0gbmV3IEhlYWRlcnMoeyAneC1hdXRoLXRva2VuJzogY3VycmVudFVzZXIudG9rZW4gfSk7XG5cdFx0XHRyZXR1cm4gbmV3IFJlcXVlc3RPcHRpb25zKHsgaGVhZGVyczogaGVhZGVycyB9KTtcblx0XHR9XG5cdH1cbn1cbiJdfQ==